<?php

class ApiTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */

	public function testGetAllNotifications()
	{
		// get all notifications and see if we get back a success status	
		// we could count (assertCount) notifications as well
		$response = $this->call('GET', '/api/v1/notification');
		$json = json_decode($response->getContent());
		
		$this->assertTrue($response->isOk());
		$this->assertSame("success", $json->status);
		
	}

	public function testStoreNotification()
	{
		// post a new notification and make sure we get it back from our API
		$parameters = array("user_id" => 1, "notification" => "Unit test");
		$response = $this->call('POST', '/api/v1/notification/', $parameters);
		$json = json_decode($response->getContent());

		$this->assertTrue($response->isOk());
		$this->assertSame(true, $json->success);
		$this->assertSame("Unit test", $json->notification->notification);

	}



}
