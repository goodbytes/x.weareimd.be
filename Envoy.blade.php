@servers(['production' => 'deploybot@178.79.133.99'])

@task('deploy')
	cd /home/x.weareimd.be
	git stash
	git pull origin master
	php composer.phar dump-autoload
	php artisan migrate --force
	php artisan db:seed --force
@endtask
