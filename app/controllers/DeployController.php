<?php

class DeployController extends BaseController {

	// creating our deployment workflow in a URL makes it really easy to trigger deployment remotely
	// however, make sure that you password protect this route in order to avoid unauthorized deployments
	public function index()
	{
		SSH::into('production')->run(array(
		    // 'php composer.phar install',
		    // php artisan migrate
		    // php artisan db::refresh
		    'cd /home/x.weareimd.be',
		    'git stash',
		    'git pull origin master',
		    'php composer.phar dump-autoload',
		    'php artisan migrate --force',
		    'php artisan db:seed --force'
		), function($line){
    		echo $line.PHP_EOL; // outputs server feedback
		});
	}

}
